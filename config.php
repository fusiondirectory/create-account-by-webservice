<?php

/* Connection information. Fill peer_name with the name matching the certificate. */
$host       = 'http://192.168.10.200/fusiondirectory/jsonrpc.php';
$ca_file    = '';
$login      = 'fd-admin';
$password   = 'secret';
$templatedn = 'cn=template_signup,ou=templates,ou=people,ou=example,ou=com';
$ssl_options = array(
  'cafile'            => $ca_file,
  'peer_name'         => 'localhost',
  'verify_peer'       => TRUE,
  'verify_peer_name'  => TRUE,
);
$http_options = array(
  'timeout' => 10
);

$domain     = 'fusiondirectory';
