<?php

/* You can find this file in FusionDirectory include directory if argonaut plugin is installed */
require_once('jsonRPCClient.php');

/* Configuration */
require_once('config.php');

$error_messages     = array();
$warning_messages   = array();
$success_messages   = array();
$form_fields        = array();
$captcha_questions  = array(
  array(_('What is the name of the software we are developing?'), _('FusionDirectory')),
  array(_('What is the version number you can read in the top-right corner?'), $version),
);

function addField(array $obj, $input = 'text')
{
  global $form_fields;
  $form_fields[] = array(
    'label'       => $obj['label'],
    'description' => $obj['description'],
    'htmlid'      => $obj['htmlid'],
    'input'       => $input,
    'value'       => (isset($_POST[$obj['htmlid']]) ? $_POST[$obj['htmlid']] : $obj['value'])
  );
}

try {
  if (!isset($http_options['header'])) {
    $http_options['header'] = '';
  }
  if (!empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
    $http_options['header'] .= 'Accept-Language: '.$_SERVER['HTTP_ACCEPT_LANGUAGE']."\r\n";
  }
  if (!empty($_SERVER['HTTP_ACCEPT_CHARSET'])) {
    $http_options['header'] .= 'Accept-Charset: '.$_SERVER['HTTP_ACCEPT_CHARSET']."\r\n";
  }
  /* We create the connection object */
  $client = new jsonRPCClient($host, $http_options, $ssl_options);
  /* Then we need to login. Here we log in the default LDAP */
  $session_id = $client->login(NULL, $login, $password);
  /* Get template fields */
  $result = $client->gettemplate($session_id, 'user', $templatedn);

  if (isset($result['errors'])) {
    $error_messages = $result['errors'];
  } else {
    $values = array();
    /* For each tab */
    foreach ($result as $tab => $infos) {
      $tabValues = array();
      /* For each attribute */
      foreach ($infos['attrs_order'] as $field) {
        $obj = $infos['attrs'][$field];
        if (!$obj['visible'] || $obj['disabled']) {
          continue;
        }
        /* Store POST value */
        foreach ($obj['type'] as $type) {
          switch ($type) {
            case 'UserPasswordAttribute':
              /* Note: password needs special treatment, pass array('', value1, value2, '', FALSE) */
              addField($obj['attributes']['userPassword_password'], 'password');
              addField($obj['attributes']['userPassword_password2'], 'password');
              if (isset($_POST[$obj['attributes']['userPassword_password']['htmlid']]) &&
                  isset($_POST[$obj['attributes']['userPassword_password2']['htmlid']])) {
                $tabValues[$field] = array(
                  '',
                  $_POST[$obj['attributes']['userPassword_password']['htmlid']],
                  $_POST[$obj['attributes']['userPassword_password2']['htmlid']],
                  '',
                  FALSE
                );
              }
              continue 3;
            case 'StringAttribute':
              addField($obj);
              if (isset($_POST[$obj['htmlid']])) {
                $tabValues[$field] = $_POST[$obj['htmlid']];
              }
              continue 3;
            case 'PasswordAttribute':
              addField($obj, 'password');
              if (isset($_POST[$obj['htmlid']])) {
                $tabValues[$field] = $_POST[$obj['htmlid']];
              }
              continue 3;
            case 'SetAttribute':
              $subObj = $obj['attributes'][$obj['attributes_order'][0]];
              if (in_array('StringAttribute', $subObj['type'])) {
                /* We treat SetAttribute[StringAttribute] as StringAttribute and send only one value */
                addField($subObj);
                if (isset($_POST[$subObj['htmlid']])) {
                  $tabValues[$field] = array($_POST[$subObj['htmlid']]);
                }
                continue 3;
              } else {
                break;
              }
            case 'BaseSelectorAttribute':
              /* TODO: Allow to set or display Base */
              continue 3;
          }
        }
        $error_messages[] = 'Type '.$obj['type'][0].' is not supported yet';
      }
      if (!empty($tabValues)) {
        $values[$tab] = $tabValues;
      }
    }
    /* Handle CAPTCHA question/answer */
    if (isset($_POST['captchaquestion'])) {
      $question     = base64_decode($_POST['captchaquestion']);
      $question_id  = NULL;
      foreach ($captcha_questions as $id => $infos) {
        if ($infos[0] == $question) {
          $question_id = $id;
          break;
        }
      }
      if ($question_id === NULL) {
        $error_messages[] = _('Invalid POST captcha data');
        $question_id = mt_rand(0, count($captcha_questions));
      }
    } else {
      $question_id = mt_rand(0, count($captcha_questions));
    }
    $hidden_fields  .= '<input type="hidden" id="captchaquestion" name="captchaquestion" value="'.base64_encode($captcha_questions[$question_id][0]).'"/>'."\n";
    $captcha_answer = (isset($_POST['captchaanswer']) ? $_POST['captchaanswer'] : '');
    $form_fields[] = array(
      'label'       => $captcha_questions[$question_id][0],
      'description' => _('Answer this question to prove you are human'),
      'htmlid'      => 'captchaanswer',
      'input'       => 'text',
      'value'       => $captcha_answer,
    );
    if (!empty($values)) {
      if (trim(strtolower($captcha_answer)) != trim(strtolower($captcha_questions[$question_id][1]))) {
        $warning_messages[] = _('Wrong captcha answer');
      } else {
        /* Send $values to usetemplate */
        $result = $client->usetemplate($session_id, 'user', $templatedn, $values);

        if (isset($result['errors'])) {
          /* These errors usually means something the user entered was invalid
           * We show them as warnings */
          $warning_messages = $result['errors'];
        } else {
          $userdn             = $result;
          $success_messages[] = $success_text;
          /* Get login of created user */
          $result = $client->getfields($session_id, 'user', $userdn);
          if (isset($result['errors'])) {
            /* This should not happen */
            $warning_messages = $result['errors'];
          } else {
            $info_text = sprintf($login_text, $result['sections']['account']['attrs']['uid']['value']);
          }
        }
      }
    }
  }
} catch (jsonRPCClient_RequestErrorException $e) {
  $error_messages[] = $e->getMessage();
} catch (jsonRPCClient_NetworkErrorException $e) {
  $error_messages[] = $e->getMessage();
}

if (!empty($warning_messages) && empty($success_messages)) {
  $info_text = '';
}
