<?php
  $lang = i18n::initLanguage(__DIR__.'/locale/', $domain);

  $title          = _('FusionDirectory - Sign up');
  $version        = _('1.1');
  $altimg         = _('FusionDirectory Icon');
  $info_text      = _('Please fill out the form below and click submit');
  $success_text   = _('Your account has been created');
  $login_text     = _('Your login is %s');
  $rtl            = i18n::isRtl($lang);
  $hidden_fields  = '';

  require('webservice.php');
  require('i18n.php');

  function output($str)
  {
    echo htmlentities($str, ENT_COMPAT | ENT_HTML5, 'UTF-8');
  }
?>
<!DOCTYPE html>
<?php if($rtl) { ?>
<html xmlns="http://www.w3.org/1999/xhtml" dir="rtl" class="rtl">
<?php } else { ?>
<html xmlns="http://www.w3.org/1999/xhtml" class="ltr">
<?php } ?>

  <head>
    <title><?php output($title); ?></title>

    <meta charset="utf-8"/>

    <meta name="description" content="<?php output($title); ?>"/>
    <meta name="author" lang="en" content="FusionDirectory Project"/>
    <meta name="viewport" content="width=device-width"/>

    <link rel="stylesheet" type="text/css" href="themes/breezy/style.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="themes/breezy/form.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="themes/breezy/datepicker.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="themes/breezy/menu.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="themes/breezy/lists.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="themes/breezy/tabs.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="themes/breezy/plugin.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="themes/breezy/setup.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="themes/breezy/theme.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="themes/breezy/dialog.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="themes/breezy/login.css" media="screen"/>

    <link rel="shortcut icon" href="favicon.ico"/>

  </head>

  <body>
    <div>
      <div class="setup-header">
        <div id="header-left">
          <img id="fd-logo" class="optional" src="img/fusiondirectory.png" alt="FusionDirectory"/>
        </div>
        <div id="header-right">
          <div class="version">
            <?php output($version); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="errors">
<?php foreach ($error_messages as $message) { ?>
      <div class="error">
        <img src="img/dialog-error.png" class="center" alt="<?php echo _('Error'); ?>"/>
        <?php output($message); ?>
      </div>
<?php } ?>
    </div>

    <div id="window-container">

      <div id="window-div">

        <form action="index.php" method="post" name="mainform">

        <div id="window-titlebar">
          <p>
            <img class="center" src="img/icon.png" alt="<?php output($altimg); ?>"/>
            <?php output($title); ?>
          </p>
        </div>
        <div id="window-content">
          <div>
            <!-- Display warning message on demand -->
<?php foreach ($warning_messages as $message) { ?>
            <p class="warning"> <?php output($message); ?> </p>
<?php } ?>
<?php foreach ($success_messages as $message) { ?>
            <div class="success">
              <img class="center" src="img/dialog-information.png" alt="<?php echo _('Success'); ?>" title="<?php echo _('Success'); ?>"/>
              <?php output($message); ?>
            </div>
<?php } ?>
            <p class="infotext">
              <?php output($info_text); ?>
            </p>
            <br/>
              <?php foreach ($form_fields as $field) { ?>
                <label for="<?php output($field['htmlid']); ?>" title="<?php output($field['description']); ?>">
                  <?php output($field['label']); ?>
                  <input
                    name="<?php output($field['htmlid']); ?>"
                    id="<?php output($field['htmlid']); ?>"
                    value="<?php output($field['value']); ?>"
                    autocomplete="off"
                    type="<?php output($field['input']); ?>"/>
                </label>
                <br/>
                <br/>
              <?php } ?>
              <?php echo $hidden_fields; ?>
            <br/>
          </div>
        </div>
        <div id="window-footer" class="plugbottom">
          <div>
          </div>
          <div>
            <input type="submit" name="apply" value="<?php echo _('Submit'); ?>" title="<?php echo _('Send the form'); ?>"/>
          </div>
        </div>
        </form>
      </div>
    </div>
  </body>
</html>
